﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject objectToDestroy;    
    public GameObject prefab;
    // Start is called before the first frame update
    public void ClearLines() {
       objectToDestroy = GameObject.Find("ParentLines");
       Destroy(objectToDestroy);          
       /* foreach(Transform child in objectToDestroy.transfrom) {
          Destroy(child.gameObject);          
       } */
    }

    public void ReloadScene() {
       SceneManager.LoadScene("Pintar");
    }

    public void DuplicarCanva(){

       Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity);
    }


}
