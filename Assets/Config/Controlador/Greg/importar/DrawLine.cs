﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour
{

    public static DrawLine drawLine;
    public GameObject linePrefab;
    public GameObject currentLine;
    public LineRenderer lineRender;
    public EdgeCollider2D edgeCollider;
    public List<Vector2> fingerPositions;
    public Camera cam;
    public bool randomColor;
    public GameObject parentLines;
	

    public bool press;
    public int num = 0;
    public static DrawLine instance;
    public GameObject linePrefabBlack, linePrefabBlue, linePrefabRed, linePrefabYellow, linePrefabGreen, linePrefabOrange, linePrefabPink;

    // Start is called before the first frame update
    void Start()
    {
randomColor = false;
        drawLine = this;
        instance = this;
        num = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0) && press)
        {
            CreateLine();
            

        }
         if(Input.GetMouseButton(0) && press)
        {
            Vector2 tempFingerPos = cam.ScreenToWorldPoint(Input.mousePosition);
            if(Vector2.Distance(tempFingerPos, fingerPositions[fingerPositions.Count-1]) > .1f){
                UpdateLine(tempFingerPos);
            }
        } 
    }

    void CreateLine(){

        //Event   currentEvent = Event.current;
        //Vector2 mousePos = new Vector2();

        // Get the mouse position from Event.
        // Note that the y position from Event is inverted.
        //mousePos.x = currentEvent.mousePosition.x;
        //mousePos.y = cam.pixelHeight - currentEvent.mousePosition.y;

if(randomColor) {

num++;

        if (num == 0)
        {
            currentLine = Instantiate(linePrefab, Vector3.zero, Quaternion.identity);

        }
        else if (num == 1)
        {
            currentLine = Instantiate(linePrefabRed, Vector3.zero, Quaternion.identity);
        }
        else if (num == 2)
        {
            currentLine = Instantiate(linePrefabBlue, Vector3.zero, Quaternion.identity);

        }
        else if (num == 3)
        {
            currentLine = Instantiate(linePrefabYellow, Vector3.zero, Quaternion.identity);
        }
        else if (num == 4)
        {
            currentLine = Instantiate(linePrefabGreen, Vector3.zero, Quaternion.identity);
            
        }
        else if (num == 5)
        {
            currentLine = Instantiate(linePrefabOrange, Vector3.zero, Quaternion.identity);
            
        }
        else if (num == 6)
        {
            currentLine = Instantiate(linePrefabPink, Vector3.zero, Quaternion.identity);
            num = 0;
        }
}else {
 currentLine = Instantiate(linePrefabBlack, Vector3.zero, Quaternion.identity);
}
        currentLine.transform.parent = parentLines.transform;

        lineRender = currentLine.GetComponent<LineRenderer>();
        edgeCollider = currentLine.GetComponent<EdgeCollider2D>();
        fingerPositions.Clear();        
        fingerPositions.Add(cam.ScreenToWorldPoint(Input.mousePosition));
        fingerPositions.Add(cam.ScreenToWorldPoint(Input.mousePosition));
        lineRender.SetPosition(0, fingerPositions[0]);
        lineRender.SetPosition(1, fingerPositions[1]);
        edgeCollider.points = fingerPositions.ToArray();
    }

    void UpdateLine(Vector2 newFingerPos){
        fingerPositions.Add(newFingerPos);
        lineRender.positionCount++;
        lineRender.SetPosition(lineRender.positionCount-1, newFingerPos);
        edgeCollider.points = fingerPositions.ToArray();

    }
}
