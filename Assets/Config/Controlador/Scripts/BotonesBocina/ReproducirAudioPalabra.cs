﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReproducirAudioPalabra : MonoBehaviour
{
    private AudioSource soundPlayer; 

    public AudioClip[] sonidosPalabras;
    
    void Awake()
    {
        soundPlayer = GetComponent<AudioSource>();
    }

    public void ReproducirAudio(){
        soundPlayer.clip = sonidosPalabras[Random.Range(0, sonidosPalabras.Length)];
        if(!soundPlayer.isPlaying)
        {
        soundPlayer.PlayOneShot(soundPlayer.clip);
        }
    }
}
