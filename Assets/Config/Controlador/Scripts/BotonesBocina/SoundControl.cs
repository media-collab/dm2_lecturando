using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControl : MonoBehaviour
{
    public static SoundControl SndMan; 
    //public AudioSource soundPlayer; 
    
    public AudioSource audioSrc;

    private AudioClip[] sonidosPalabras;

    private int palabraRandom;


    // Start is called before the first frame update
    void Start()
    {
        SndMan = this;
        audioSrc = GetComponent<AudioSource>();
        sonidosPalabras = Resources.LoadAll<AudioClip>("Palabras");
    }


    public void ReproducirPalabra(){
       // soundPlayer.Play();
        palabraRandom = Random.Range(0,3);
        audioSrc.PlayOneShot(sonidosPalabras[palabraRandom]);
           
    }
}
