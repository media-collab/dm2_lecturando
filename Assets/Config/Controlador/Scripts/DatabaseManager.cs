﻿using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DatabaseManager : MonoBehaviour
{
	public InputField Name;
	public InputField Pass;
	public InputField NameSession;
	public InputField PassSession;
	public InputField NameRegister;
	public InputField PassRegister;
	private string userID;
	private DatabaseReference dbRef;
	public Text NameText;
	public Text PassText;
	public GameObject Alert;

	[Header("List of scores and names")]
    // Create list of text Text to store the data of name and score 
    public List<Text> nameTexts = new List<Text>();
    // public List<Text> scoreTexts = new List<Text>();

    // Start is called before the first frame update
    void Start()
    {
		userID= SystemInfo.deviceUniqueIdentifier;
        dbRef = FirebaseDatabase.DefaultInstance.RootReference;
	}

 	public void CreateUser()
    {
		User new_user= new User(Name.text, Pass.text, "student", "0",0,0,0,0,0);
		string json = JsonUtility.ToJson(new_user);
		dbRef.Child("users").Child(userID).SetRawJsonValueAsync(json);
		Debug.Log("Usuario creado");
		Alert.SetActive(true);
    }

 	public void CreateTeacher()
    {
		User new_user= new User(NameRegister.text, PassRegister.text, "teacher", "0",0,0,0,0,0);
		string json = JsonUtility.ToJson(new_user);
		dbRef.Child("users").Child(userID).SetRawJsonValueAsync(json);
		Debug.Log("Profesor creado");
		Alert.SetActive(true);
    }

	public void GetUserInfo() {
		FirebaseDatabase.DefaultInstance
		.GetReference("users")
		.GetValueAsync().ContinueWithOnMainThread(task => {
			if (task.IsFaulted) {
			// Handle the error...
			}
			else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;
				// Do something with snapshot...
				
				// Debug.Log(snapshot.GetRawJsonValue());
				
				

				// string json2 = snapshot.GetRawJsonValue();
        		// User user2 = (User)JsonUtility.FromJson(json2, typeof(User));
				// Debug.Log(user2.name);
				// Debug.Log(user2.pass);


				
				// Debug.Log("noc" + JsonUtility.FromJson<User>(json2).name);

				// obtener los valores individuales del json {"3d220d70d48b96f5fd15903523ffff4b846ac5a0":{"name":"dsdsa","pass":23}}
				string name = snapshot.Child(userID).Child("name").Value.ToString();
				string pass = snapshot.Child(userID).Child("pass").Value.ToString();
				string type = snapshot.Child(userID).Child("type").Value.ToString();
				
				int amountErrors = int.Parse(snapshot.Child(userID).Child("amountErrors").Value.ToString());
				int amountErrorsLetterD = int.Parse(snapshot.Child(userID).Child("amountErrorsLetterD").Value.ToString());
				int amountErrorsLetterB = int.Parse(snapshot.Child(userID).Child("amountErrorsLetterB").Value.ToString());
				int enemiesDefeated = int.Parse(snapshot.Child(userID).Child("enemiesDefeated").Value.ToString());
				int amountAttempts = int.Parse(snapshot.Child(userID).Child("amountAttempts").Value.ToString());

				Debug.Log("name: " + name);
				Debug.Log("pass: " + pass);


				string username = NameSession.text;
				string userpass = (PassSession.text);

				if(username == name && userpass == pass && type == "student") {
					// Clear prefabs
					PlayerPrefs.DeleteAll();
					PlayerPrefs.SetString("Username",username);
					PlayerPrefs.SetString("Userpass",userpass);
					PlayerPrefs.SetInt("amountErrors",amountErrors);
					PlayerPrefs.SetInt("amountErrorsLetterD",amountErrorsLetterD);
					PlayerPrefs.SetInt("amountErrorsLetterB",amountErrorsLetterB);
					PlayerPrefs.SetInt("enemiesDefeated",enemiesDefeated);
					PlayerPrefs.SetInt("amountAttempts", amountAttempts);
					PlayerPrefs.Save();
					//Debug.Log(PlayerPrefs.GetString("Username"));
					//Debug.Log(PlayerPrefs.GetString("Userpass"));
					SceneManager.LoadScene("Personajes");
					Debug.Log("Login correcto");
					
				} else {
					GetAllUsers(username, userpass);
				}
			}
		});


		
	}


	public void GetAllUsers(string value1 , string value2 ) {

		FirebaseDatabase.DefaultInstance
		.GetReference("users")
		.GetValueAsync().ContinueWithOnMainThread(task => {
			if (task.IsFaulted) {
			// Handle the error...
                Debug.Log("Error");
			}
			else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;

                // Save the data of name and score in list of text 
                List<string> nameList = new List<string>();
                List<string> passList = new List<string>();
				
				
				
				foreach (DataSnapshot childSnapshot in snapshot.Children) {
					// Debug.Log(childSnapshot.GetRawJsonValue());
					string json2 = childSnapshot.GetRawJsonValue();
					User user2 = (User)JsonUtility.FromJson(json2, typeof(User));

						if(value1 == user2.name && value2 == (user2.pass) && user2.type == "teacher") {
							
							SceneManager.LoadScene("Condiciones");
						}
						
				}

				Debug.Log("Login incorrecto");

               
			}
					
		});		
	}
}
