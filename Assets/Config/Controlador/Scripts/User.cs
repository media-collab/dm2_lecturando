﻿public class User
{
    public string name;
    public string pass;
    public string type;
    public string score;
    public int enemiesDefeated;
    public int amountErrors;
    public int amountAttempts;
    public int amountErrorsLetterD;
    public int amountErrorsLetterB;

    public User(string name, string pass, string type, string score, int enemiesDefeated, int amountErrors, int amountErrorsLetterD, int amountErrorsLetterB, int amountAttempts)
    {
        this.name = name;
        this.pass = pass;
        this.type = type;
        this.score = score;
        this.enemiesDefeated = enemiesDefeated;
        this.amountErrors = amountErrors;
        this.amountErrors = amountAttempts;
        this.amountErrorsLetterD = amountErrorsLetterD;
        this.amountErrorsLetterB = amountErrorsLetterB;
    }
}
