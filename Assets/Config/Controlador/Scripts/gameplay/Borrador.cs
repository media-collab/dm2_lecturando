using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Borrador : MonoBehaviour
{
    public GameObject Boton1;
    public GameObject Boton2;
    public GameObject Boton3;
    public GameObject Boton4;

    public void Enable()
    {
        Boton1.SetActive(true);
        Boton2.SetActive(true);
        Boton3.SetActive(true);
        Boton4.SetActive(true);
    }
}
