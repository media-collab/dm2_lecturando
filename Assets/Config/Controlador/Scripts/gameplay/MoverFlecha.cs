﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverFlecha : MonoBehaviour
{
    public Transform target;
    public Transform target2;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
  

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            GetComponent<AudioSource>().Play();
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            GetComponent<AudioSource>().Play();
            transform.position = Vector3.MoveTowards(transform.position, target2.position, speed);
        }

    }

    public void startRight(){
        GetComponent<AudioSource>().Play();
            transform.position = Vector3.MoveTowards(transform.position, target.position, speed);
    }

    public void startLeft(){
            GetComponent<AudioSource>().Play();
            transform.position = Vector3.MoveTowards(transform.position, target2.position, speed);
    }

 
    

    
}
