﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveImage : MonoBehaviour
{
     public int resWidth = 64; 
     public int resHeight = 64;
 
     private bool takeHiResShot = false;

    //  List os SpriteRenderer
    public List<SpriteRenderer> spriteRenderList = new List<SpriteRenderer>();
 
     public static string ScreenShotName(int width, int height, int id) {
        string fullName = "";
        switch (id) {
            case 0:
                fullName = "reto1letra1";
                break;
            case 1:
                fullName = "reto1letra2";
                break;
                default:
                fullName = "default";
                break;
        }

         return string.Format("{0}/screenshots/{3}.png", 
                              Application.dataPath, 
                              width, height, 
                              fullName);
     }

     public void TakeHiResShot() {
         takeHiResShot = true;
     }
 
     void LateUpdate() {
         takeHiResShot |= Input.GetKeyDown("k");
         if (takeHiResShot) {
             RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
             GetComponent<Camera>().targetTexture = rt;
             Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
             GetComponent<Camera>().Render();
             RenderTexture.active = rt;
             screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
             GetComponent<Camera>().targetTexture = null;
             RenderTexture.active = null; // JC: added to avoid errors
             Destroy(rt);
             byte[] bytes = screenShot.EncodeToPNG();
             string filename = ScreenShotName(resWidth, resHeight, -1);
             System.IO.File.WriteAllBytes(filename, bytes);
             Debug.Log(string.Format("Took screenshot to: {0}", filename));
             takeHiResShot = false;
         }
     }

     public void SavePNG() {
             RenderTexture rt = new RenderTexture(8, 8, 24);
             GetComponent<Camera>().targetTexture = rt;
             Texture2D screenShot = new Texture2D(8, 8, TextureFormat.RGB24, false);
             GetComponent<Camera>().Render();
             RenderTexture.active = rt;
             screenShot.ReadPixels(new Rect(0, 0, 8, 8), 0, 0);
             GetComponent<Camera>().targetTexture = null;
             RenderTexture.active = null; // JC: added to avoid errors
             Destroy(rt);
             byte[] bytes = screenShot.EncodeToPNG();
             string filename = ScreenShotName(8, 8, -1);
             System.IO.File.WriteAllBytes(filename, bytes);
             Debug.Log(string.Format("Took screenshot to: {0}", filename));
             takeHiResShot = false;


    }

    // save sprite render to png
    public void SaveSpriteRender(int idSp) {
        Texture2D texture = new Texture2D(spriteRenderList[idSp].sprite.texture.width, spriteRenderList[idSp].sprite.texture.height, TextureFormat.RGB24, false);
        texture.SetPixels(spriteRenderList[idSp].sprite.texture.GetPixels());
        texture.Apply();
        byte[] bytes = texture.EncodeToPNG();
        string filename = ScreenShotName(8, 8, idSp);
        System.IO.File.WriteAllBytes(filename, bytes);
        Debug.Log(string.Format("Took screenshot to: {0}", filename));
        takeHiResShot = false;
    }
}
