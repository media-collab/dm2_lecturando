using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cargarPuntos : MonoBehaviour
{
    [SerializeField] private Text textoPuntaje;
    [SerializeField] private Text textoFallos;
    [SerializeField] private Text textoEnemigos;
    // Start is called before the first frame update
    void Start()
    {
        int puntajeNumero = PlayerPrefs.GetInt("puntaje");
        textoPuntaje.text = string.Format("{0}", puntajeNumero);
        int fallosNumero = PlayerPrefs.GetInt("amountErrors");
        textoFallos.text = string.Format("{0}", fallosNumero);
        int enemigosNumero = PlayerPrefs.GetInt("enemiesDefeated");
        textoEnemigos.text = string.Format("{0}", enemigosNumero);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
 
}
