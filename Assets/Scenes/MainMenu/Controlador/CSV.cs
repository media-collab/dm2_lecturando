using System.Collections;
using System.Collections.Generic;
using System.IO;
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using UnityEngine;

public class CSV : MonoBehaviour
{
    string filename;

    // Start is called before the first frame update
    void Start()
    {
        filename = Application.dataPath + "/test.csv";
        // WriteCSV();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void WriteCSV()
    {
        FirebaseDatabase
            .DefaultInstance
            .GetReference("users")
            .GetValueAsync()
            .ContinueWithOnMainThread(task =>
            {
                if (task.IsFaulted)
                {
                    // Handle the error...
                    Debug.Log("Error");
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;

                    // Save the data of name and score in list of text
                    List<string> nameList = new List<string>();
                    List<string> scoreList = new List<string>();

                    TextWriter tw = new StreamWriter(filename, false);
                    tw
                        .WriteLine("Nombre;Puntaje;Cantidad de enemigos derrotados;Cantidad de errores en la letra B;Cantidad de errores en la letra D;Cantidad de errores;Cantidad de intentos");
                    tw.Close();

                    tw = new StreamWriter(filename, true);

                    // Get all the users from the database
                    foreach (DataSnapshot user in snapshot.Children)
                    {
                        tw
                            .WriteLine(user.Child("name").Value.ToString() +
                            ";" +
                            user.Child("score").Value.ToString() +
                            ";" +
                            user.Child("enemiesDefeated").Value.ToString() +
                            ";" +
                            user.Child("amountErrorsLetterB").Value.ToString() +
                            ";" +
                            user.Child("amountErrorsLetterD").Value.ToString() +
                            ";" +
                            user.Child("amountErrors").Value.ToString() +
                            ";" +
                            user.Child("amountAttempts").Value.ToString());
                    }
                    tw.Close();

                    Debug.Log("---- Finish Review ----");
                }
            });
    }
}
