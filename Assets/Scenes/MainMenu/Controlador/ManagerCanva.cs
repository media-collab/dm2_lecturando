using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerCanva : MonoBehaviour
{
    public GameObject canvaPieChart;
    public GameObject canvaTable;
    public static ManagerCanva instance;
    public Text nameStudent;
    public Text scoreStudent;
    public PieChart pieChartLetters;
    public PieChart pieChartErrorPercentage;
    private int amountErrorsLetterB;
    private int amountErrorsLetterD;
    private int amountErrors;
    private int amountAttempts;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;

    }

    public void ActivarPieChart()
    {
        canvaPieChart.SetActive(true);
        canvaTable.SetActive(false);
        nameStudent.text = PlayerPrefs.GetString("Dataname");
        scoreStudent.text = PlayerPrefs.GetString("Datascore");
        amountErrorsLetterB = PlayerPrefs.GetInt("DataamountErrorsLetterB");
        amountErrorsLetterD = PlayerPrefs.GetInt("DataamountErrorsLetterD");
        amountErrors = PlayerPrefs.GetInt("DataamountErrors");
        amountAttempts = PlayerPrefs.GetInt("DataamountAttempts");
        pieChartLetters.SetValues(CalculateBDErrors());
        pieChartErrorPercentage.SetValues(CalculateTotalErrors());
        PieChart.instance.UpdatePieChart();
    }

    public void ActivarTable()
    {
        canvaPieChart.SetActive(false);
        canvaTable.SetActive(true);
    }

    public float[] CalculateBDErrors()
    {
        float[] lettersErrorPercentage = new float[2];
        float letterErrorSum = amountErrorsLetterB + amountErrorsLetterD;

        lettersErrorPercentage[0] = (float)(letterErrorSum * 100) / amountErrors;
        lettersErrorPercentage[1] = 100 - lettersErrorPercentage[0];

        Debug.Log(amountErrorsLetterB);
        Debug.Log(amountErrorsLetterD);
        Debug.Log(amountErrors);

        return lettersErrorPercentage;
    }

    public float[] CalculateTotalErrors()
    {
        float[] errorPercentageValues = new float[2];

        errorPercentageValues[0] = (float)(amountErrors * 100) / amountAttempts;
        errorPercentageValues[1] = 100 - errorPercentageValues[0];


        return errorPercentageValues;
    }
}
