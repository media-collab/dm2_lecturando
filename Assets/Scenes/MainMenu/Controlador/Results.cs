using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Results : MonoBehaviour
{

    private string userID;
	private DatabaseReference dbRef;

    public List<Text> nameTexts = new List<Text>();

    public void Start()
    {
        Debug.Log("nameList[i]");


        FirebaseDatabase.DefaultInstance
		.GetReference("users")
		.GetValueAsync().ContinueWithOnMainThread(task => {
			if (task.IsFaulted) {
			// Handle the error...
                Debug.Log("Error");
			}
			else if (task.IsCompleted) {
				DataSnapshot snapshot = task.Result;

                // Save the data of name and score in list of text 
                List<string> nameList = new List<string>();
                List<string> scoreList = new List<string>();

                // create index
                int index = 0;
                // Get all the users from the database
                foreach (DataSnapshot user in snapshot.Children)
                {
                    nameList.Add(user.Child("name").Value.ToString());
                    scoreList.Add(user.Child("score").Value.ToString());
                }
                // Set the name and score to the text
                for (int i = 0; i < nameList.Count; i++)
                {
                    string textito = "Usuario: " + nameList[i] + " - Puntaje: " + scoreList[i];
                    nameTexts[i].text = textito;
                }
			}
		});	
    }

}
