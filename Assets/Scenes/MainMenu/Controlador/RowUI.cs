using System.Collections;
using System.Collections.Generic;
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using UnityEngine;
using UnityEngine.UI;

public class RowUI : MonoBehaviour
{
    public Text rank;

    public Text name;

    public Text score;

    public string btnReview;

    public void OnClickReview()
    {
        Debug.Log("---- Review ----" + btnReview);
        PlayerPrefs.SetString("nameStudent", btnReview);
        GetSingleData();
    }

    public void GetSingleData()
    {
        FirebaseDatabase
            .DefaultInstance
            .GetReference("users")
            .GetValueAsync()
            .ContinueWithOnMainThread(task =>
            {
                if (task.IsFaulted)
                {
                    // Handle the error...
                    Debug.Log("Error");
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;

                    // Save the data of name and score in list of text
                    List<string> nameList = new List<string>();
                    List<string> scoreList = new List<string>();

                    // Get all the users from the database
                    foreach (DataSnapshot user in snapshot.Children)
                    {
                        if (
                            PlayerPrefs.GetString("nameStudent") ==
                            user.Child("name").Value.ToString()
                        )
                        {
                            // Save the data of name and score in list of text
                            // nameList.Add(user.Child("name").Value.ToString());
                            // scoreList.Add(user.Child("score").Value.ToString());
                            PlayerPrefs
                                .SetString("Dataname",
                                user.Child("name").Value.ToString());
                            PlayerPrefs
                                .SetString("Datascore",
                                user.Child("score").Value.ToString());
                            PlayerPrefs
                                .SetInt("DataenemiesDefeated",
                                int
                                    .Parse(user
                                        .Child("enemiesDefeated")
                                        .Value
                                        .ToString()));
                            PlayerPrefs
                                .SetInt("DataamountErrorsLetterB",
                                int
                                    .Parse(user
                                        .Child("amountErrorsLetterB")
                                        .Value
                                        .ToString()));
                            PlayerPrefs
                                .SetInt("DataamountErrorsLetterD",
                                int
                                    .Parse(user
                                        .Child("amountErrorsLetterD")
                                        .Value
                                        .ToString()));
                            PlayerPrefs
                                .SetInt("DataamountErrors",
                                int
                                    .Parse(user
                                        .Child("amountErrors")
                                        .Value
                                        .ToString()));
                            PlayerPrefs
                                .SetInt("DataamountAttempts",
                                int
                                    .Parse(user
                                        .Child("amountAttempts")
                                        .Value
                                        .ToString()));
                        }
                    }

                    Debug.Log("---- Finish Review ----");
                    ManagerCanva.instance.ActivarPieChart();
                }
            });
    }
}
