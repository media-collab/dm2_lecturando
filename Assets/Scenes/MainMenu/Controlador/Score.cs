using System;

[Serializable]
public class Score
{
    public string name;

    public float score;

    public float attempts;

    public Score(string name, float score, float attempts)
    {
        this.name = name;
        this.score = score;
        this.attempts = attempts;
    }
}
