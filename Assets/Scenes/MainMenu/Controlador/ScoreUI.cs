using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using UnityEngine;

public class ScoreUI : MonoBehaviour
{
    [Header("UI Config")]
    public RowUI rowUi;

    public ScoreManager scoreManager;

    [Header("Firebase Config")]
    private DatabaseReference dbRef;

    private string userID;

    public void Awake()
    {
        userID = SystemInfo.deviceUniqueIdentifier;
        dbRef = FirebaseDatabase.DefaultInstance.RootReference;
        GetStudentsInformation();
    }

    public void RenderData()
    {
        // scoreManager.AddScore(new Score("Player 3", 500));
        // scoreManager.AddScore(new Score("Player 4", 600));
        // scoreManager.AddScore(new Score("Player 5", 700));
        // scoreManager.AddScore(new Score("Player 6", 800));
        // scoreManager.AddScore(new Score("Player 7", 900));
        var scores = scoreManager.GetHighScores().ToArray();
        for (int i = 0; i < scores.Length; i++)
        {
            var row = Instantiate(rowUi, transform).GetComponent<RowUI>();

            // row.rank.text = (i + 1).ToString();
            row.rank.text = scores[i].name; // Name
            row.name.text = scores[i].score.ToString(); // Score
            row.score.text = scores[i].attempts.ToString(); // Amount Errors
            row.btnReview = scores[i].name; // Data to save
        }
    }

    public void GetStudentsInformation()
    {
        PlayerPrefs.DeleteAll();

        FirebaseDatabase
            .DefaultInstance
            .GetReference("users")
            .GetValueAsync()
            .ContinueWithOnMainThread(task =>
            {
                if (task.IsFaulted)
                {
                    // Handle the error...
                    Debug.Log("Error");
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;

                    // Save the data of name and score in list of text
                    List<string> nameList = new List<string>();
                    List<string> scoreList = new List<string>();

                    // Get all the users from the database
                    foreach (DataSnapshot user in snapshot.Children)
                    {
                        // Save the data of name and score in list of text
                        nameList.Add(user.Child("name").Value.ToString());
                        scoreList.Add(user.Child("score").Value.ToString());

                        /*  Debug
                            .Log("name: " +
                            user.Child("name").Value.ToString()); */
                        scoreManager
                            .AddScore(new Score(user
                                    .Child("name")
                                    .Value
                                    .ToString(),
                                float
                                    .Parse(user
                                        .Child("score")
                                        .Value
                                        .ToString()),
                                float
                                    .Parse(user
                                        .Child("amountErrors")
                                        .Value
                                        .ToString())));
                    }

                    Debug.Log("nameList: " + nameList.Count);
                    RenderData();
                }
            });
    }
}
