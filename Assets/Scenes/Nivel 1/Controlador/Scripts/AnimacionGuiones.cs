using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimacionGuiones : MonoBehaviour
{

       public int numero;

    public SpriteRenderer rend;
    public SpriteRenderer rend2;
    public SpriteRenderer rend3;
    public SpriteRenderer rend4;
    public SpriteRenderer rend5;
    public Button Skip;
    private bool skip = false;

    public GameObject interfaz;

    // Start is called before the first frame update
    void Start()
    {

        Button btn = Skip.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
        //rend = GetComponent<SpriteRenderer>();
        Color c = rend.material.color;
        c.a = 0f;
        rend.material.color = c;
        rend2.material.color = c;
        rend3.material.color = c;
        rend4.material.color = c;
        rend5.material.color = c;

        interfaz.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {

        //while (skip == false)
        //{
            numero = numero + 1;

            if (numero == 500)
            {
                StartCoroutine("Guion1");
            }

            if (numero == 1500)
            {
                StartCoroutine("Guion2");
            }

            if (numero == 2500)
            {
                StartCoroutine("GuionesOut");
            }

            if (numero == 3000)
            {
                StartCoroutine("Guion3In");
            }


            //PERSONAJES --------------------------------------------------------------

            if (numero == 200)
            {
                StartCoroutine("FutbolistaIn");
                StartCoroutine("AlienIn");
            }



            // INTERFAZ-----------------------------------------------------------------

            //if (numero == 3900)
            //{
            //    interfaz.SetActive(true);
            //}
        //}


    }

        IEnumerator Guion1()
    {
        for (float f = 0.05f; f<= 1; f += 0.05f)
        {
            Color c = rend.material.color;
            c.a = f;
            rend.material.color = c;
            yield return new WaitForSeconds(0.05f);
        }
    }

            IEnumerator Guion2()
    {
        for (float f = 0.05f; f<= 1; f += 0.05f)
        {
            Color c = rend.material.color;
            c.a = f;
            rend2.material.color = c;
            yield return new WaitForSeconds(0.05f);
        }
    }

    
        IEnumerator GuionesOut()
    {
        for(float f=1f; f>=-0.05f; f -= 0.05f)
        {
            Color c = rend.material.color;
            c.a = f;
            rend.material.color = c;
            rend2.material.color = c;
            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator Guion3In()
    {
    for (float f = 0.05f; f<= 1; f += 0.05f)
        {
            Color c = rend.material.color;
            c.a = f;
            rend5.material.color = c;
            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator FutbolistaIn()
    {
        for (float f = 0.05f; f<= 1; f += 0.05f)
        {
            Color c = rend.material.color;
            c.a = f;
            rend3.material.color = c;
            yield return new WaitForSeconds(0.05f);
        }
    }

        IEnumerator AlienIn()
    {
        for (float f = 0.05f; f<= 1; f += 0.05f)
        {
            Color c = rend.material.color;
            c.a = f;
            rend4.material.color = c;
            yield return new WaitForSeconds(0.05f);
        }
    }
    private void TaskOnClick()
    {
        //skip = true;
        //Color c = rend.material.color;
        //c.a = 0f;
        //rend.material.color = c;
        //rend2.material.color = c;
        //rend5.material.color = c;
        //StartCoroutine("GuionesOut");

        //interfaz.SetActive(true);

    }

}
