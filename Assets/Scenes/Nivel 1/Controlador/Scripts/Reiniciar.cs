using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.SceneManagement;

public class Reiniciar : MonoBehaviour
{
[SerializeField] Button BotonReiniciar;
    public void ReiniciarNivel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }
    // Start is called before the first frame update
    void Start()
    {
        BotonReiniciar.onClick.AddListener(ReiniciarNivel);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
