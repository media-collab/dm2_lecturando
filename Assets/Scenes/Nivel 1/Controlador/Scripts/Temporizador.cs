using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.SceneManagement;

public class Temporizador : MonoBehaviour
{
    public float TimeLeft;
    public bool TimerOn = false;

    public Text TiempoTexto;

    [SerializeField] Button BotonReiniciar;
   public Image BotonReiniciarImage;
    public bool BotonReiniciarActivo;
    // Start is called before the first frame update
    void Start()
    {
        TimerOn=true;
       
     BotonReiniciarImage.gameObject.SetActive(false);
     BotonReiniciarActivo = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (TimerOn)
        {
           if(TimeLeft > 0)
           {
            TimeLeft -= Time.deltaTime;
            updateTimer(TimeLeft);
           }
              else
              {
                Debug.Log("Se acabó el tiempo! Pierde, paila, perdió lenguaje, cero");
                BotonReiniciarImage.gameObject.SetActive(true);
                BotonReiniciarActivo = true;
                //EditorUtility.DisplayDialog("Se acabó el tiempo!", "Pierde, paila, perdió lenguaje, cero", "OK");
                //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                TimeLeft = 0;
                TimerOn = false;
              }
        }


    }

    void updateTimer(float currentTime){
        currentTime += 1;

        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);

        TiempoTexto.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
