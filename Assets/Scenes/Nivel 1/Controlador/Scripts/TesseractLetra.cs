using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;

public class TesseractLetra : MonoBehaviour
{
    [SerializeField] private Texture2D imageToRecognize;

    [SerializeField] private Text displayText;
    [SerializeField] public Image popUpNoReconoce;

    //[SerializeField] private RawImage outputImage;
    private TesseractDriver _tesseractDriver;
    private string _text = "";

    private Texture2D _texture;
    public Button yourButton;

    [SerializeField] private AudioSource soundPlayer;
    [SerializeField] public AudioClip[] sonidosPalabras;


    int puntajeNumero;

    void Awake()
    {
        soundPlayer = GetComponent<AudioSource>();
    }

    void Start()
    {
        popUpNoReconoce.gameObject.SetActive(false);
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    private void TaskOnClick()
    {
        Texture2D texture = new Texture2D(imageToRecognize.width, imageToRecognize.height, TextureFormat.ARGB32, false);
        texture.SetPixels32(imageToRecognize.GetPixels32());

        texture.Apply();

        _tesseractDriver = new TesseractDriver();
        Recoginze(texture);

        LetterValidation();
    }

    private void LetterValidation()
    {


        if ((!_text.Contains("d")) && (!_text.Contains("b")) && (!_text.Contains("p")))
        {
            Debug.Log("Letra no reconocida, �intenta de nuevo!");
            popUpNoReconoce.gameObject.SetActive(true);

            soundPlayer.PlayOneShot(sonidosPalabras[0]);
        }

        else
        {
            Debug.Log("Se reconoci� una de las letras: d, b, p");
        }
        Debug.Log("------------------- ");
        Debug.Log("Letra: " + _text);
    }

    private void Recoginze(Texture2D outputTexture)
    {
        _texture = outputTexture;
        ClearTextDisplay();
        _tesseractDriver.Setup(OnSetupCompleteRecognize);
    }

    private void OnSetupCompleteRecognize()
    {
        AddToTextDisplay(_tesseractDriver.Recognize(_texture));
        AddToTextDisplay(_tesseractDriver.GetErrorMessage(), true);
        //SetImageDisplay();
    }

    private void ClearTextDisplay()
    {
        _text = "";
    }

    private void AddToTextDisplay(string text, bool isError = false)
    {
        if (string.IsNullOrWhiteSpace(text)) return;

        _text += (string.IsNullOrWhiteSpace(displayText.text) ? "" : "\n") + text.ToLower().Replace(" ", string.Empty);

        if (isError)
            Debug.LogError(text);
    }

    private void LateUpdate()
    {
        displayText.text = _text;
    }
}