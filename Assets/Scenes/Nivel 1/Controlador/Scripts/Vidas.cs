using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vidas : MonoBehaviour
{
    //VIDAS
    [SerializeField] public GameObject[] VidasNivel;
    [SerializeField] int vida;


    // Start is called before the first frame update
    void Start()
    {
        vida = VidasNivel.Length;
    }

    // Update is called once per frame
    void Update()
    {
        if (vida == 3)
        {
            VidasNivel[0].SetActive(true);
            VidasNivel[1].SetActive(true);
            VidasNivel[2].SetActive(true);
        }
        if (vida == 2)
        {
            VidasNivel[0].SetActive(true);
            VidasNivel[1].SetActive(true);
            VidasNivel[2].SetActive(false);
        }
        if (vida == 1)
        {
            VidasNivel[0].SetActive(true);
            VidasNivel[1].SetActive(false);
            VidasNivel[2].SetActive(false);
        }
        if (vida == 0)
        {
            VidasNivel[0].SetActive(false);
            VidasNivel[1].SetActive(false);
            VidasNivel[2].SetActive(false);
        }
    }
}
