using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;

public class TesseractNivel2 : MonoBehaviour
{
    [SerializeField] private Texture2D imageToRecognize;
    [SerializeField] private Texture2D imageToRecognize2;
    [SerializeField] private Texture2D imageToRecognize3;

    [SerializeField] private Text displayText;
    [SerializeField] private Text displayText2;
    [SerializeField] private Text displayText3;

    [SerializeField] public Image popUpNoReconoce;
    [SerializeField] public Image popUpPerdio;
    [SerializeField] public Image popUpGano;

    //VIDAS
    [SerializeField] public GameObject[] vidasNivel;
    [SerializeField] private int vida;

    [SerializeField] private Animator AlienAtaque;
    [SerializeField] private string AlienAtaqueTrigger = "AlienAtaque";
    [SerializeField] private Animator TextoPuntos;
    [SerializeField] private string TextoPuntosTrigger = "TextoPuntos";
    [SerializeField] private Text TextoPuntosTexto;
    [SerializeField] private Animator FutbolistaHerido;
    [SerializeField] private string FutbolistaHeridoTrigger = "FutbolistaHerido";


    //[SerializeField] private RawImage outputImage;
    private TesseractDriver _tesseractDriver;
    private string _text = "";
    private string _text2 = "";
    private string _text3 = "";

    public int puntaje = 0;

    private Texture2D _texture;
    private Texture2D _texture2;
    private Texture2D _texture3;

    public Button yourButton;
    private bool victoria;


    private string userID;
    private DatabaseReference dbRef;


    public SpriteRenderer rend;
    public AudioSource rayo;


    //Ataque Futbolista

    [SerializeField] public GameObject video;
    [SerializeField] public GameObject canva;
    [SerializeField] public GameObject alienMuerto;

    [SerializeField] private Text textoPuntaje;
    [SerializeField] private AudioSource soundPlayer;
    [SerializeField] public AudioClip[] sonidosPalabras;

    
    //Ataque Alien

    [SerializeField] public GameObject personaje1;
    [SerializeField] public GameObject personaje2;


    int puntajeNumero;

    void Awake()
    {
        soundPlayer = GetComponent<AudioSource>();
    }

    void Start()
    {
        //VIDA
        vida = 3;

        int puntajeNumero = PlayerPrefs.GetInt("puntaje");
        popUpNoReconoce.gameObject.SetActive(false);
        popUpPerdio.gameObject.SetActive(false);
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);

        userID = SystemInfo.deviceUniqueIdentifier;
        dbRef = FirebaseDatabase.DefaultInstance.RootReference;

        //Animacion Ataque
        video.SetActive(false);
        canva.SetActive(true);
    }

    private void CheckLife()
    {
        if (vida < 1)
        {
            Destroy(vidasNivel[0].gameObject);
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
        else if (vida < 2)
        {
            Destroy(vidasNivel[1].gameObject);	
        }
        else if (vida < 3)
        {
            Destroy(vidasNivel[2].gameObject);	
        }
        
    }

    void habilitarOn()
    {
        canva.SetActive(true);
        alienMuerto.SetActive(false);
    }

    void habilitarOff()
    {
        canva.SetActive(false);
    }
    void popUpInvoke()
    {
        popUpPerdio.gameObject.SetActive(true);
        soundPlayer.PlayOneShot(sonidosPalabras[1]); //Aqu� debe ir la voz de perdiste
    }

    void PopUpGanador()
    {
        popUpGano.gameObject.SetActive(true);
    }

    void alienAtaque()
    {
        rend.sortingOrder = -1;
        rend.enabled = false;
        AlienAtaque.CrossFadeInFixedTime("Alien2Ataque", 1);
        AlienAtaque.SetBool(AlienAtaqueTrigger, true);
        AlienAtaque.Play(AlienAtaqueTrigger);
        AlienAtaque.SetBool(AlienAtaqueTrigger, false);
        FutbolistaHerido.CrossFadeInFixedTime("FutbolistaHerido", 1);
        FutbolistaHerido.SetBool(FutbolistaHeridoTrigger, true);
        FutbolistaHerido.Play(FutbolistaHeridoTrigger);
        FutbolistaHerido.SetBool(FutbolistaHeridoTrigger, false);
        rayo.PlayDelayed(0.5f);
        rend.enabled = true;
        rend.sortingOrder = 1;
    }


    //-------------------------- Cierre Animacion Ataque-------------

     //******************Animaciones Daño--------------------------

        void habilitarOffAlienAtack()
    {
        personaje1.SetActive(false);
        personaje2.SetActive(false);
    }
            void habilitarOnAlienAtack()
    {
        personaje1.SetActive(true);
        personaje2.SetActive(true);
    }


    private void TaskOnClick()
    {
        Texture2D texture = new Texture2D(imageToRecognize.width, imageToRecognize.height, TextureFormat.ARGB32, false);
        Texture2D texture2 = new Texture2D(imageToRecognize2.width, imageToRecognize2.height, TextureFormat.ARGB32, false);
        Texture2D texture3 = new Texture2D(imageToRecognize3.width, imageToRecognize3.height, TextureFormat.ARGB32, false);

        texture.SetPixels32(imageToRecognize.GetPixels32());
        texture2.SetPixels32(imageToRecognize2.GetPixels32());
        texture3.SetPixels32(imageToRecognize3.GetPixels32());

        texture.Apply();
        texture2.Apply();
        texture3.Apply();

        _tesseractDriver = new TesseractDriver();
        Recoginze(texture, texture2, texture3);

        LetterValidation();
    }

    private void LetterValidation()
    {


        if (((!_text.Contains("d")) && (!_text.Contains("b")) && (!_text.Contains("p"))) ||
            ((!_text2.Contains("d")) && (!_text2.Contains("b")) && (!_text2.Contains("p"))) ||
            ((!_text3.Contains("d")) && (!_text3.Contains("b")) && (!_text3.Contains("p"))))
        {
            Debug.Log("Letra no reconocida, �intenta de nuevo!");
            vida -= 1;
            CheckLife();
            popUpNoReconoce.gameObject.SetActive(true);

            soundPlayer.PlayOneShot(sonidosPalabras[0]);
        }

        else
        {
            if (!_text.Contains("p") && _text2.Contains("b") && _text3.Contains("d"))
            {
                Debug.Log("�Error! Te equivocaste de letra: p");
                vida -= 1;
                CheckLife();
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);

            }

            else if (_text.Contains("p") && !_text2.Contains("b") && _text3.Contains("d"))
            {
                Debug.Log("�Error! Te equivocaste de letra: b");
                vida -= 1;
                CheckLife();
                PlayerPrefs.SetInt("amountErrorsLetterB", PlayerPrefs.GetInt("amountErrorsLetterB") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (_text.Contains("p") && _text2.Contains("b") && !_text3.Contains("d"))
            {
                Debug.Log("�Error! Te equivocaste de letra: d");
                vida -= 1;
                CheckLife();
                PlayerPrefs.SetInt("amountErrorsLetterD", PlayerPrefs.GetInt("amountErrorsLetterD") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (!_text.Contains("p") && !_text2.Contains("b") && _text3.Contains("d"))
            {
                Debug.Log("�Error! Te equivocaste de letra: p, b");
                vida -= 1;
                CheckLife();
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterB", PlayerPrefs.GetInt("amountErrorsLetterB") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (!_text.Contains("p") && _text2.Contains("b") && !_text3.Contains("d"))
            {
                Debug.Log("�Error! Te equivocaste de letra: p, d");
                vida -= 1;
                CheckLife();
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterD", PlayerPrefs.GetInt("amountErrorsLetterD") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (_text.Contains("p") && !_text2.Contains("b") && !_text3.Contains("d"))
            {
                Debug.Log("�Error! Te equivocaste de letra: b, d");
                vida -= 1;
                CheckLife();
                PlayerPrefs.SetInt("amountErrorsLetterB", PlayerPrefs.GetInt("amountErrorsLetterB") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterD", PlayerPrefs.GetInt("amountErrorsLetterD") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (!_text.Contains("p") && !_text2.Contains("b") && !_text3.Contains("d"))
            {
                Debug.Log("�Error! Te equivocaste en las tres letras.");
                vida -= 1;
                CheckLife();
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterB", PlayerPrefs.GetInt("amountErrorsLetterB") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterD", PlayerPrefs.GetInt("amountErrorsLetterD") + 1);

                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else
            {
                video.SetActive(true);
                Invoke("habilitarOff", 1.0f);
                Invoke("habilitarOn", 4.0f);

                PlayerPrefs.SetInt("enemiesDefeated", PlayerPrefs.GetInt("enemiesDefeated") + 1);
                Debug.Log("�Excelente! escribiste la palabra de la forma correcta.");
                victoria = true;
                puntaje = 100;
                vida += 3;
                CheckLife();
                TextoPuntosTexto.gameObject.SetActive(true);
                TextoPuntos.CrossFadeInFixedTime("TextoPuntos", 1);
                TextoPuntos.SetBool(TextoPuntosTrigger, true);
                TextoPuntos.Play(TextoPuntosTrigger);
                TextoPuntos.SetBool(TextoPuntosTrigger, false);
                
                Invoke("PopUpGanador", 4.0f);

                Scene scene = SceneManager.GetActiveScene();

                if (scene.name == "1Pobladora")
                {
                    puntajeNumero += 25;
                    PlayerPrefs.SetInt("puntaje", puntajeNumero);

                    textoPuntaje.text = string.Format("{0}", puntajeNumero);
                }
                if (scene.name == "2Aprobados")
                {
                    // int puntajeNumero = PlayerPrefs.GetInt("puntaje");

                    puntajeNumero += 25;

                    textoPuntaje.text = string.Format("{0}", puntajeNumero);
                }
                if (scene.name == "3Prohibido")
                {
                    // int puntajeNumero = PlayerPrefs.GetInt("puntaje");

                    puntajeNumero += 25;

                    textoPuntaje.text = string.Format("{0}", puntajeNumero);
                }

                //SceneManager.LoadScene("ResumenFinal");
                //Invoke("cargarEscena", 6.0f);
            }
        }

        PlayerPrefs.SetInt("amountErrors", PlayerPrefs.GetInt("amountErrors") + 1);

        User new_user = new User(
            PlayerPrefs.GetString("Username"),
            PlayerPrefs.GetString("Userpass"),
            "student",
            puntaje.ToString(),
            PlayerPrefs.GetInt("enemiesDefeated"),
            PlayerPrefs.GetInt("amountErrors"),
            PlayerPrefs.GetInt("amountErrorsLetterD"),
            PlayerPrefs.GetInt("amountErrorsLetterB"),
            PlayerPrefs.GetInt("amountAttempts")
        );

        PlayerPrefs.SetFloat("score", puntaje);
        string json = JsonUtility.ToJson(new_user);
        dbRef.Child("users").Child(userID).SetRawJsonValueAsync(json);
        //Debug.Log(PlayerPrefs.GetString("Username"));
        //Debug.Log(PlayerPrefs.GetString("Userpass"));
        Debug.Log("Usuario actualizado");
        Debug.Log("Valor de victoria: " + victoria);
        Debug.Log("Valor de puntaje: " + puntaje);

        Debug.Log("------------------- ");
        Debug.Log("Texto 1: " + _text);
        Debug.Log("Texto 2: " + _text2);
        Debug.Log("Texto 3: " + _text3);
        Debug.Log("------------------- ");

    }

    private void Recoginze(Texture2D outputTexture, Texture2D outputTexture2, Texture2D outputTexture3)
    {
        _texture = outputTexture;
        _texture2 = outputTexture2;
        _texture3 = outputTexture3;
        ClearTextDisplay();
        _tesseractDriver.Setup(OnSetupCompleteRecognize);
    }

    private void OnSetupCompleteRecognize()
    {
        AddToTextDisplay(_tesseractDriver.Recognize(_texture), _tesseractDriver.Recognize(_texture2), _tesseractDriver.Recognize(_texture3));
        AddToTextDisplay(_tesseractDriver.GetErrorMessage(), _tesseractDriver.GetErrorMessage(), _tesseractDriver.GetErrorMessage(), true);
        //SetImageDisplay();
    }

    private void ClearTextDisplay()
    {
        _text = "";
        _text2 = "";
    }

    private void AddToTextDisplay(string text, string text2, string text3, bool isError = false)
    {
        if (string.IsNullOrWhiteSpace(text)) return;
        if (string.IsNullOrWhiteSpace(text2)) return;
        if (string.IsNullOrWhiteSpace(text3)) return;

        _text += (string.IsNullOrWhiteSpace(displayText.text) ? "" : "\n") + text.ToLower().Replace(" ", string.Empty);
        _text2 += (string.IsNullOrWhiteSpace(displayText2.text) ? "" : "\n") + text2.ToLower().Replace(" ", string.Empty);
        _text3 += (string.IsNullOrWhiteSpace(displayText3.text) ? "" : "\n") + text3.ToLower().Replace(" ", string.Empty);

        if (isError)
            Debug.LogError(text);
    }

    private void LateUpdate()
    {
        displayText.text = _text;
        displayText2.text = _text2;
        displayText3.text = _text3;
    }

}