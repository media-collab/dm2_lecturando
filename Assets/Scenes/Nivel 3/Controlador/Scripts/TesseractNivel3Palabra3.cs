﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;

public class TesseractNivel3Palabra3 : MonoBehaviour
{
    [SerializeField] private Texture2D imageToRecognize;
    [SerializeField] private Texture2D imageToRecognize2;
    [SerializeField] private Texture2D imageToRecognize3;
    [SerializeField] private Texture2D imageToRecognize4;

    [SerializeField] private Text displayText;
    [SerializeField] private Text displayText2;
    [SerializeField] private Text displayText3;
    [SerializeField] private Text displayText4;


    [SerializeField] public Image popUpNoReconoce;
    [SerializeField] public Image popUpPerdio;
    [SerializeField] public Image popUpGano;

    [SerializeField] private Animator AlienAtaque;
    [SerializeField] private string AlienAtaqueTrigger = "JefeAtaque";
    [SerializeField] private Animator TextoPuntos;
    [SerializeField] private string TextoPuntosTrigger = "TextoPuntos";
    [SerializeField] private Text TextoPuntosTexto;
    [SerializeField] private Animator FutbolistaHerido;
    [SerializeField] private string FutbolistaHeridoTrigger = "FutbolistaHerido";


    //[SerializeField] private RawImage outputImage;
    private TesseractDriver _tesseractDriver;
    private string _text = "";
    private string _text2 = "";
    private string _text3 = "";
    private string _text4 = "";

    public int puntaje = 0;

    private Texture2D _texture;
    private Texture2D _texture2;
    private Texture2D _texture3;
    private Texture2D _texture4;

    public Button yourButton;
    private bool victoria;

    private string userID;
    private DatabaseReference dbRef;

    public SpriteRenderer rend;
    public AudioSource rayo;


    //Ataque Futbolista

    [SerializeField] public GameObject video;
    [SerializeField] public GameObject canva;
    [SerializeField] public GameObject alienMuerto;

    [SerializeField] private Text textoPuntaje;
    [SerializeField] private AudioSource soundPlayer;
    [SerializeField] public AudioClip[] sonidosPalabras;

            //Ataque Alien

    [SerializeField] public GameObject personaje1;
    [SerializeField] public GameObject personaje2;



    int puntajeNumero;

    void Awake()
    {
        soundPlayer = GetComponent<AudioSource>();
    }

    void Start()
    {
        int puntajeNumero = PlayerPrefs.GetInt("puntaje");
        popUpNoReconoce.gameObject.SetActive(false);
        popUpPerdio.gameObject.SetActive(false);
        Button btn = yourButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);

        userID = SystemInfo.deviceUniqueIdentifier;
        dbRef = FirebaseDatabase.DefaultInstance.RootReference;

        //Animacion Ataque
        video.SetActive(false);
        canva.SetActive(true);
    }

    void habilitarOn()
    {
        canva.SetActive(true);
        alienMuerto.SetActive(false);
    }

    void habilitarOff()
    {
        canva.SetActive(false);
    }

    
            void habilitarOffAlienAtack()
    {
        personaje1.SetActive(false);
        personaje2.SetActive(false);
    }
            void habilitarOnAlienAtack()
    {
        personaje1.SetActive(true);
        personaje2.SetActive(true);
    }
    void popUpInvoke()
    {
        popUpPerdio.gameObject.SetActive(true);
        soundPlayer.PlayOneShot(sonidosPalabras[1]); //Aqu� debe ir la voz de perdiste
    }

    void PopUpGanador()
    {
        popUpGano.gameObject.SetActive(true);
    }

    void alienAtaque()
    {
        rend.sortingOrder = -1;
        rend.enabled = false;
        AlienAtaque.CrossFadeInFixedTime("JefeAtaque", 1);
        AlienAtaque.SetBool(AlienAtaqueTrigger, true);
        AlienAtaque.Play(AlienAtaqueTrigger);
        AlienAtaque.SetBool(AlienAtaqueTrigger, false);
        FutbolistaHerido.CrossFadeInFixedTime("FutbolistaHerido", 1);
        FutbolistaHerido.SetBool(FutbolistaHeridoTrigger, true);
        FutbolistaHerido.Play(FutbolistaHeridoTrigger);
        FutbolistaHerido.SetBool(FutbolistaHeridoTrigger, false);
        rayo.PlayDelayed(0.5f);
        rend.enabled = true;
        rend.sortingOrder = 1;
    }

    private void TaskOnClick()
    {
        Texture2D texture = new Texture2D(imageToRecognize.width, imageToRecognize.height, TextureFormat.ARGB32, false);
        Texture2D texture2 = new Texture2D(imageToRecognize2.width, imageToRecognize2.height, TextureFormat.ARGB32, false);
        Texture2D texture3 = new Texture2D(imageToRecognize3.width, imageToRecognize3.height, TextureFormat.ARGB32, false);
        Texture2D texture4 = new Texture2D(imageToRecognize4.width, imageToRecognize4.height, TextureFormat.ARGB32, false);

        texture.SetPixels32(imageToRecognize.GetPixels32());
        texture2.SetPixels32(imageToRecognize2.GetPixels32());
        texture3.SetPixels32(imageToRecognize3.GetPixels32());
        texture4.SetPixels32(imageToRecognize4.GetPixels32());

        texture.Apply();
        texture2.Apply();
        texture3.Apply();
        texture4.Apply();

        _tesseractDriver = new TesseractDriver();
        Recoginze(texture, texture2, texture3, texture4);

        LetterValidation();
    }

    private void LetterValidation()
    {


        if (((!_text.Contains("d")) && (!_text.Contains("b")) && (!_text.Contains("p"))) ||
            ((!_text2.Contains("d")) && (!_text2.Contains("b")) && (!_text2.Contains("p"))) ||
            ((!_text3.Contains("d")) && (!_text3.Contains("b")) && (!_text3.Contains("p"))) ||
            ((!_text4.Contains("d")) && (!_text4.Contains("b")) && (!_text4.Contains("p"))))
        {
            Debug.Log("Letra no reconocida, ¡intenta de nuevo!");
            popUpNoReconoce.gameObject.SetActive(true);
            soundPlayer.PlayOneShot(sonidosPalabras[0]);
        }

        else
        {
            if (!_text.Contains("p") && _text2.Contains("p") && _text3.Contains("d") && _text4.Contains("b"))
            {
                Debug.Log("�Error! Te equivocaste de letra: p");
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);

            }

            else if (_text.Contains("p") && !_text2.Contains("p") && _text3.Contains("d") && _text4.Contains("b"))
            {
                Debug.Log("�Error! Te equivocaste de letra: p");
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (_text.Contains("p") && _text2.Contains("p") && !_text3.Contains("d") && _text4.Contains("b"))
            {
                Debug.Log("¡Error! Te equivocaste de letra: d");
                PlayerPrefs.SetInt("amountErrorsLetterD", PlayerPrefs.GetInt("amountErrorsLetterD") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (_text.Contains("p") && _text2.Contains("p") && _text3.Contains("d") && !_text4.Contains("b"))
            {
                Debug.Log("¡Error! Te equivocaste de letra: b");
                PlayerPrefs.SetInt("amountErrorsLetterB", PlayerPrefs.GetInt("amountErrorsLetterB") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (!_text.Contains("p") && !_text2.Contains("p") && _text3.Contains("d") && _text4.Contains("b"))
            {
                Debug.Log("�Error! Te equivocaste de letra: p, p");
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 2);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (!_text.Contains("p") && _text2.Contains("p") && !_text3.Contains("d") && _text4.Contains("b"))
            {
                Debug.Log("�Error! Te equivocaste de letra: p, d");
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterD", PlayerPrefs.GetInt("amountErrorsLetterD") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (!_text.Contains("p") && _text2.Contains("p") && _text3.Contains("d") && !_text4.Contains("b"))
            {
                Debug.Log("�Error! Te equivocaste de letra: p, b");
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterB", PlayerPrefs.GetInt("amountErrorsLetterB") + 1);

                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (_text.Contains("p") && !_text2.Contains("p") && !_text3.Contains("d") && _text4.Contains("b"))
            {
                Debug.Log("�Error! Te equivocaste de letra: p, d");
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterD", PlayerPrefs.GetInt("amountErrorsLetterD") + 1);

                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (_text.Contains("p") && !_text2.Contains("p") && _text3.Contains("d") && !_text4.Contains("b"))
            {
                Debug.Log("�Error! Te equivocaste de letra: p, b");
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterB", PlayerPrefs.GetInt("amountErrorsLetterB") + 1);

                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (_text.Contains("p") && _text2.Contains("p") && !_text3.Contains("d") && !_text4.Contains("b"))
            {
                Debug.Log("�Error! Te equivocaste de letra: d, b");
                PlayerPrefs.SetInt("amountErrorsLetterD", PlayerPrefs.GetInt("amountErrorsLetterD") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterB", PlayerPrefs.GetInt("amountErrorsLetterB") + 1);
                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else if (!_text.Contains("p") && !_text2.Contains("p") && !_text3.Contains("d") && !_text4.Contains("b"))
            {
                Debug.Log("�Error! Te equivocaste en todas las letras.");
                PlayerPrefs.SetInt("amountErrorsLetterP", PlayerPrefs.GetInt("amountErrorsLetterP") + 2);
                PlayerPrefs.SetInt("amountErrorsLetterD", PlayerPrefs.GetInt("amountErrorsLetterD") + 1);
                PlayerPrefs.SetInt("amountErrorsLetterB", PlayerPrefs.GetInt("amountErrorsLetterB") + 1);

                victoria = false;
                alienAtaque();
                Invoke("popUpInvoke", 3.0f);
                                                Invoke("habilitarOffAlienAtack", 0f);
                Invoke("habilitarOnAlienAtack", 3f);
            }

            else
            {
                video.SetActive(true);
                Invoke("habilitarOff", 1.0f);
                Invoke("habilitarOn", 4.0f);

                PlayerPrefs.SetInt("enemiesDefeated", PlayerPrefs.GetInt("enemiesDefeated") + 1);
                Debug.Log("�Excelente! escribiste la palabra de la forma correcta.");
                victoria = true;
                puntaje = 100;
                TextoPuntosTexto.gameObject.SetActive(true);
                TextoPuntos.CrossFadeInFixedTime("TextoPuntos", 1);
                TextoPuntos.SetBool(TextoPuntosTrigger, true);
                TextoPuntos.Play(TextoPuntosTrigger);
                TextoPuntos.SetBool(TextoPuntosTrigger, false);

                Invoke("PopUpGanador", 4.0f);

                Scene scene = SceneManager.GetActiveScene();

                if (scene.name == "1Publicidad")
                {
                    puntajeNumero += 50;
                    PlayerPrefs.SetInt("puntaje", puntajeNumero);

                    textoPuntaje.text = string.Format("{0}", puntajeNumero);
                }
                if (scene.name == "2Depredador")
                {
                    // int puntajeNumero = PlayerPrefs.GetInt("puntaje");

                    puntajeNumero +=50;

                    textoPuntaje.text = string.Format("{0}", puntajeNumero);
                }
                if (scene.name == "3Parpadeaba")
                {
                    // int puntajeNumero = PlayerPrefs.GetInt("puntaje");

                    puntajeNumero += 50;

                    textoPuntaje.text = string.Format("{0}", puntajeNumero);
                }

                //SceneManager.LoadScene("ResumenFinal");
                //Invoke("cargarEscena", 6.0f);
            }
        }

        PlayerPrefs.SetInt("amountErrors", PlayerPrefs.GetInt("amountErrors") + 1);

        User new_user = new User(
            PlayerPrefs.GetString("Username"),
            PlayerPrefs.GetString("Userpass"),
            "student",
            puntaje.ToString(),
            PlayerPrefs.GetInt("enemiesDefeated"),
            PlayerPrefs.GetInt("amountErrors"),
            PlayerPrefs.GetInt("amountErrorsLetterD"),
            PlayerPrefs.GetInt("amountErrorsLetterB"),
            PlayerPrefs.GetInt("amountAttempts")
        );

        PlayerPrefs.SetFloat("score", puntaje);
        string json = JsonUtility.ToJson(new_user);
        dbRef.Child("users").Child(userID).SetRawJsonValueAsync(json);
        //Debug.Log(PlayerPrefs.GetString("Username"));
        //Debug.Log(PlayerPrefs.GetString("Userpass"));
        Debug.Log("Usuario actualizado");
        Debug.Log("Valor de victoria: " + victoria);
        Debug.Log("Valor de puntaje: " + puntaje);

        Debug.Log("-------------------");
        Debug.Log("Texto 1: " + _text);
        Debug.Log("Texto 2: " + _text2);
        Debug.Log("Texto 3: " + _text3);
        Debug.Log("Texto 4: " + _text3);

    }

    private void Recoginze(Texture2D outputTexture, Texture2D outputTexture2, Texture2D outputTexture3, Texture2D outputTexture4)
    {
        _texture = outputTexture;
        _texture2 = outputTexture2;
        _texture3 = outputTexture3;
        _texture4 = outputTexture4;

        ClearTextDisplay();
        _tesseractDriver.Setup(OnSetupCompleteRecognize);
    }

    private void OnSetupCompleteRecognize()
    {
        AddToTextDisplay(_tesseractDriver.Recognize(_texture), _tesseractDriver.Recognize(_texture2), _tesseractDriver.Recognize(_texture3), _tesseractDriver.Recognize(_texture4));
        AddToTextDisplay(_tesseractDriver.GetErrorMessage(), _tesseractDriver.GetErrorMessage(), _tesseractDriver.GetErrorMessage(), _tesseractDriver.GetErrorMessage(), true);
    }

    private void ClearTextDisplay()
    {
        _text = "";
        _text2 = "";
        _text3 = "";
        _text4 = "";

    }

    private void AddToTextDisplay(string text, string text2, string text3, string text4, bool isError = false)
    {
        if (string.IsNullOrWhiteSpace(text)) return;
        if (string.IsNullOrWhiteSpace(text2)) return;
        if (string.IsNullOrWhiteSpace(text3)) return;
        if (string.IsNullOrWhiteSpace(text4)) return;

        _text += (string.IsNullOrWhiteSpace(displayText.text) ? "" : "\n") + text.ToLower().Replace(" ", string.Empty);
        _text2 += (string.IsNullOrWhiteSpace(displayText2.text) ? "" : "\n") + text2.ToLower().Replace(" ", string.Empty);
        _text3 += (string.IsNullOrWhiteSpace(displayText3.text) ? "" : "\n") + text3.ToLower().Replace(" ", string.Empty);
        _text4 += (string.IsNullOrWhiteSpace(displayText4.text) ? "" : "\n") + text4.ToLower().Replace(" ", string.Empty);

        if (isError)
            Debug.LogError(text);
    }

    private void LateUpdate()
    {
        displayText.text = _text;
        displayText2.text = _text2;
        displayText3.text = _text3;
        displayText4.text = _text4;
    }

}