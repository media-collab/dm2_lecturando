﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PieChart : MonoBehaviour
{
    public Image[] imagesPieChart;

    public float[] values;

    public static PieChart instance;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    public void UpdatePieChart()
    {
        Debug.Log("PieChart");
        // scoreText.text = PlayerPrefs.GetString("Dataname");
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetValues(float[] valuesS)
    {
        float totalValues = 0;
        for (int i = 0; i < imagesPieChart.Length; i++)
        {
            totalValues += FindPercentage(valuesS, i);
            imagesPieChart[i].fillAmount = totalValues;
        }

        values = valuesS;
    }

    private float FindPercentage(float[] valuesS, int index)
    {
        float total = 0;
        for (int i = 0; i < valuesS.Length; i++)
        {
            total += valuesS[i];
        }

        return valuesS[index] / total;
    }
}
