using Firebase.Database;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebManager : MonoBehaviour
{
    private string userID;
    private DatabaseReference dbRef;

    public PieChart pieChartLetters;
    public PieChart pieChartErrorPercentage;

    private List<float> amountErrorsLetterB = new List<float>();
    private List<float> amountErrorsLetterD = new List<float>();
    private List<float> amountErrors = new List<float>();
    private List<float> amountAttempts = new List<float>();

    private void Awake()
    {
        FirebaseDatabase.DefaultInstance
        .GetReference("users")
        .GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted)
            {
                // Handle the error...
                Debug.Log("Error");
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;

                // create index
                int index = 0;

                // Get all the users from the database
                foreach (DataSnapshot user in snapshot.Children)
                {
                    amountErrorsLetterB.Add(float.Parse(user.Child("amountErrorsLetterB").Value.ToString()));
                    amountErrorsLetterD.Add(float.Parse(user.Child("amountErrorsLetterD").Value.ToString()));
                    amountErrors.Add(float.Parse(user.Child("amountErrors").Value.ToString()));
                    //amountAttempts.Add(float.Parse(user.Child("amountAttempts").Value.ToString()));
                    //Debug.Log(amountErrors.Count);
                }
            }

            pieChartLetters.SetValues(CalculateBDErrors());
            pieChartErrorPercentage.SetValues(CalculateTotalErrors());
        });
    }

    /// <summary>
    /// The first value represents the percentage of error in letters B and D and the second value represents
    /// the error in P and Q
    /// </summary>
    /// <returns></returns>
    public float[] CalculateBDErrors()
    {
        float[] lettersErrorPercentage = new float[2];
        float lettersErrorSum = 0;
        float totalErrors = 0;
        Debug.Log(amountErrors.Count);

        for (int i = 0; i < amountErrors.Count; i++)
        {
            lettersErrorSum += amountErrorsLetterB[i] + amountErrorsLetterD[i];
            totalErrors += amountErrors[i];
        }

        lettersErrorPercentage[0] = (float)(lettersErrorSum * 100) / totalErrors;
        lettersErrorPercentage[1] = 100 - lettersErrorPercentage[0];

        Debug.Log(lettersErrorPercentage[0]);
        Debug.Log(lettersErrorPercentage[1]);

        return lettersErrorPercentage;
    }


    /// <summary>
    /// The first value represents the percentage of error and the second value represents
    /// the percentage of success
    /// </summary>
    /// <returns></returns>
    public float[] CalculateTotalErrors()
    {
        float[] errorPercentageValues = new float[2];
        float totalAttempts = 0;
        float totalErrors = 0;
        //Debug.Log(amountErrors.Count);

        for (int i = 0; i < amountErrors.Count; i++)
        {
            totalAttempts += amountAttempts[i];
            totalErrors += amountErrors[i];
        }

        errorPercentageValues[0] = (float)(totalErrors * 100) / totalAttempts;
        errorPercentageValues[1] = 100 - errorPercentageValues[0];

        return errorPercentageValues;
    }

}
