# Lecturando 🎮

Unity game focused on solving the letter inversion problem (p, q, b, d) detected in kids from schools in Cali, Colombia. Using image recognition and database integration in this gamification-based videogame, we offer solutions for teachers to help address this problem.

## 🌟 Features

- **Letter Inversion Detection**: Identify and address the inversion of letters (p, q, b, d) in children's writing.
- **Image Recognition**: Utilize Tesseract OCR for detecting letter inversions.
- **Educational Gamification**: Engage kids with a fun and educational game experience.
- **Teacher Support**: Provide tools and solutions for teachers to help students improve.

## 🚀 Technologies

- **Unity**: Game development platform.
- **GitHub & GitLab**: Version control and collaboration.
- **Miro**: Collaborative whiteboarding.
- **Scrum**: Agile project management methodology.
- **Documentation & Testing**: Ensure project quality and reliability.
- **Tesseract OCR**: Optical character recognition for image analysis.
- **ONNX & Unity Barracuda**: AI model integration.
- **Firebase**: Backend database and authentication for user management and real-time storage of scores and statistics.
- **C#**: Programming language for game development.

## 📚 Project Overview

### Research Phase

We started with an in-depth investigation of the problem, engaging with both teachers and students to understand their needs. We conducted user experience tests which informed the development process, including the creation of game characters.

### Documentation

Our documentation is based on methodologies from the book [Preproducción de sistemas multimedia (Preproduction of Multimedia Systems)](https://editorial.uao.edu.co/gpd-preproduccion-de-sistemas-multimedia.html) by Carlos Peláez, Andrés Solano, and Antoni Saltiveri. This comprehensive approach guided our preproduction phase.

### Development Phase

This repository contains the post-production phase where we developed the game. Our focus was on creating a robust and effective tool for educational purposes.

## 📦 Installation

### Prerequisites

- [Unity](https://unity.com/)
- [Git](https://git-scm.com/)

### Steps

1. **Clone the repository**
    ```sh
    git clone https://gitlab.com/xlgabriel/lecturando.git
    cd lecturando
    ```

2. **Open the project in Unity**
    - Launch Unity Hub.
    - Add the cloned project to your Unity Hub.
    - Open the project in Unity Editor.

3. **Install dependencies**
    - Ensure all required packages and dependencies are installed through Unity's Package Manager.

## 🛠️ Usage

1. **Run the Game**
    - Use the Unity Editor to play and test the game.
    - Build and deploy the game for your target platform.

2. **Engage with the Game**
    - Follow the instructions in-game to help kids with letter inversions.
    - Teachers can use the provided tools to track progress and provide assistance.

## 🏆 Incentives and Impact

The real-time storage of scores and statistics in Firebase allows teachers to make informed academic decisions. These results can serve as incentives for students to use the multimedia system, thereby mitigating the problem of letter inversion.

## 🤝 Contributing

Contributions are welcome! Please fork this repository and submit a merge request.

## 📝 License

This project is licensed under the MIT License.

## 📧 Contact

For any questions or feedback, please contact [gabriel.jeannot.personal@gmail.com](mailto:gabriel.jeannot.personal@gmail.com) or any member of this team.